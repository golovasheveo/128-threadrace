package telran;

import telran.menu.InputOutput;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.stream.IntStream;

public class RaceTableStats extends Thread {
    private static final int MIN = 2;
    private static final int MAX = 5;
    private static final Object mutex = new Object();
    public static ArrayList<Integer[]> order = new ArrayList<>();
    int distance;
    InputOutput inputOutput;

    public RaceTableStats(String name, int distance, InputOutput inputOutput) {
        super(name);
        this.inputOutput = inputOutput;
        this.distance = distance;
    }

    public static void clearStats() {
        order.clear();
    }

    @Override
    public void run() {
        Instant start = Instant.now();
        for (int i = 0; i < distance; i++) {
            inputOutput.displayLine(getName());
            try {
                sleep(getRandInt());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        synchronized (mutex) {
            int duration = (int) ChronoUnit.MILLIS.between(start, Instant.now());
            Integer[] toAdd = new Integer[2];
            toAdd[0] = Integer.parseInt(getName());
            toAdd[1] = duration;
            order.add(toAdd);
        }

    }

    private long getRandInt() {
        return (long) (Math.random() * ((long) RaceTableStats.MAX
                - (long) RaceTableStats.MIN + 1)
                + (long) RaceTableStats.MIN);
    }
}
