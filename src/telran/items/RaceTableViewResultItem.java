package telran.items;

import telran.RaceTableStats;
import telran.menu.InputOutput;
import telran.menu.Item;

public class RaceTableViewResultItem implements Item {

    private final InputOutput inputOutput;

    public RaceTableViewResultItem(InputOutput inputOutput) {
        super();
        this.inputOutput = inputOutput;
    }

    @Override
    public String displayName() {
        return "Start Threads-Race Game with table results";
    }

    @Override
    public void perform() {
        int nThreads = inputOutput.inputInteger("Enter number of threads", 2, 1000);
        int distance = inputOutput.inputInteger("Enter distance", 2, 1000);

        RaceTableStats[] threads = new RaceTableStats[nThreads];
        startThreads(nThreads, distance, threads);
        joinRacers(threads);

        printRaceStats();
        RaceTableStats.clearStats();

    }

    private void printRaceStats() {

        int place = 1;

        for (Integer[] line : RaceTableStats.order) {
            inputOutput.displayLine(String.format("Place %d,  Thread id %d, Running time %d", place, line[0], line[1]));
            place++;
        }

    }

    private void startThreads(int nThreads, int distance, RaceTableStats[] threads) {
        for (int i = 0; i < nThreads; i++) {
            threads[i] = new RaceTableStats("" + (i + 1), distance, inputOutput);
            threads[i].start();
        }
    }

    private void joinRacers(RaceTableStats[] threads) {
        for (RaceTableStats t : threads) {
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
