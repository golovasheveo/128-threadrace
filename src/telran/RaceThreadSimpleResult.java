package telran;

import telran.menu.InputOutput;

public class RaceThreadSimpleResult extends Thread {
    private static final int MIN = 2;
    private static final int MAX = 5;
    private static final Object mutex = new Object();
    private static int winner = 0;
    int distance;
    InputOutput inputOutput;

    public RaceThreadSimpleResult(String name, int distance, InputOutput inputOutput) {
        super(name);
        this.inputOutput = inputOutput;
        this.distance = distance;
    }

    public static void clearWinner() {
        winner = 0;
    }

    public static int getWinner() {
        return winner;
    }

    @Override
    public void run() {
        for (int i = 0; i < distance; i++) {
            inputOutput.displayLine(getName());
            try {
                sleep(getRandInt());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        synchronized (mutex) {
            if (winner == 0) {
                winner = Integer.parseInt(getName());
            }
        }

    }

    private long getRandInt() {
        return (long) (Math.random() * ((long) RaceThreadSimpleResult.MAX
                - (long) RaceThreadSimpleResult.MIN + 1)
                + (long) RaceThreadSimpleResult.MIN);
    }
}
